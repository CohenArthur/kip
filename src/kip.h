#include <stdint.h>
#include <stddef.h>

/**
 * @brief Default capacity for the kip_map
 */
#define DEFAULT_CAPACITY 8

/**
 * @brief Flag to indicate that the system memory is limited.
 *
 * When the map reaches capacity, allocates one more slot instead of
 * allocating twice as many slots.
 */
#define LIMITED_MEMORY 1

/**
 * @brief Generic macro to finish the initialization of a struct kip
 */
/*#define KIP_INIT\
    struct kip *new_struct kip = __malloc(sizeof(struct kip));\
    new_struct kip->pointer = new_pointer;\
    new_struct kip->kip_map = (void *) map;\
    new_struct kip->free = free;\
    __struct kip_map_size_chk(map);\
    map->array[map->size] = new_struct kip;\
    ++map->size;\
    */

/**
 * Map to store struct kips (smart pointers). Similar to a basic vector
 * @param flags		Flags for the map
 * @param capacity	Maximum number of struct kips of the map
 * @param size		Number of struct kips stored in the map
 * @param array		Array containing the struct kips
 */
struct kip_map {
    uint8_t flags;
    size_t capacity;
    size_t size;

    void **array;
};

/**
 * Inits and returns a new struct kip_map
 */
void kip_map_init(int flags);

/**
 * Destroys an existing struct kip_map
 * @param map 	Map to deallocate
 */
void kip_map_free(void);

/**
 * @brief Allocates a new struct kip and stores it in a struct kip_map
 * @param size 	Size of the new pointer, same size as you would malloc()
 * @param map		Map to store the new pointer in. Cannot be NULL.
 */
void *kip_malloc(size_t size);

/**
 * @brief Allocates a new struct kip as an array of zeros and stores it in a struct kip_map
 * @param number	Number of arrays to create
 * @param size 	Size of the new pointer, same size as you would malloc()
 * @param map		Map to store the new pointer in. Cannot be NULL.
 */
void *kip_calloc(size_t number, size_t size);

/**
 * @brief Reallocates a struct kip to a new size
 * @param old_struct kip Kip to reallocate
 * @param size New size of the struct kip's pointer, same as you would pass to realloc()
 */
void *kip_realloc(void *pointer, size_t new_size);
