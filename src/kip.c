#include <err.h>
#include <stdlib.h>
#include <sys/types.h>

#include "kip.h"

static struct kip_map *g_map_ptr = NULL;

static void *__malloc(size_t size)
{
    void *pointer = malloc(size);
    if (!pointer)
        err(1, NULL);

    return pointer;
}

static void *__calloc(size_t number, size_t size)
{
    void *pointer = calloc(number, size);
    if (!pointer)
        err(1, NULL);

    return pointer;
}

static void *__realloc(void *pointer, size_t size)
{
    void *new_pointer = realloc(pointer, size);
    if (!pointer)
        err(1, NULL);

    return new_pointer;
}

void kip_map_init(int flags)
{
    if (g_map_ptr)
        return;

    struct kip_map *new_map = __malloc(sizeof(struct kip_map));
    void **array = __malloc(sizeof(void *) * DEFAULT_CAPACITY);

    new_map->flags = flags;
    new_map->array = array;
    new_map->capacity = DEFAULT_CAPACITY;
    new_map->size = 0;

    g_map_ptr = new_map;
}

void kip_map_free(void)
{
    if (!g_map_ptr)
        errx(1, "%s", "struct kip_map_free(): freeing uninitialized map");

    for (size_t kip_index = 0; kip_index < g_map_ptr->size; ++kip_index)
        if (g_map_ptr->array[kip_index])
            free(g_map_ptr->array[kip_index]);

    free(g_map_ptr->array);
    free(g_map_ptr);
    g_map_ptr = NULL;
}

static void __kip_map_size_chk(struct kip_map *map) {
    if (map->size == map->capacity - 1) {
        if (map->flags & LIMITED_MEMORY)
            map->capacity += 1;
        else
            map->capacity *= 2;
        map->array = __realloc(map->array, map->capacity * sizeof(void *));
    }
}

static void kip_map_add(void *pointer) {
    if (!g_map_ptr || !pointer)
        return;

    __kip_map_size_chk(g_map_ptr);
    g_map_ptr->array[g_map_ptr->size] = pointer;
    ++g_map_ptr->size;
}

void *kip_malloc(size_t size)
{
    if (!g_map_ptr)
        errx(1, "%s", "kip_malloc(): global map not initialized");

    void *new_pointer = __malloc(size);

    kip_map_add(new_pointer);

    return new_pointer;
}

void *kip_calloc(size_t number, size_t size)
{
    if (!g_map_ptr)
        errx(1, "%s", "kip_calloc(): global map not initialized");

    void *new_pointer = __calloc(number, size);

    kip_map_add(new_pointer);

    return new_pointer;
}

static ssize_t find_pointer(void *pointer)
{
    for (size_t i = 0; i < g_map_ptr->size; i++)
        if (g_map_ptr->array[i] == pointer)
            return i;

    return -1;
}

void *kip_realloc(void *pointer, size_t new_size)
{
    if (!g_map_ptr)
        errx(1, "%s", "kip_realloc(): global map not initialized");

    ssize_t ptr_index = find_pointer(pointer);
    if (ptr_index == -1)
        return kip_malloc(new_size);

    g_map_ptr->array[ptr_index] = __realloc(pointer, new_size);

    return g_map_ptr->array[ptr_index];
}
