ifdef DBG
CFLAGS=-D_DBG -g3
endif

CC=clang
CFLAGS+=-Wall -Wextra -Werror -std=c11 -pedantic -fsanitize=address
LDFLAGS=-lcriterion

SRC_DIR=./src/
LIB_DIR=./lib/

ALL_SRC=$(wildcard $(SRC_DIR)*.c) $(wildcard $(LIB_DIR)*.c)

SRC=unit_tests.c ${ALL_SRC}
OBJ=${SRC:*.c=*.o}

all : unit_tests

unit_tests : ${OBJ}

clean:
	${RM} unit_tests *.o *.d
