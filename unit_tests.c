#include <criterion/criterion.h>

#include "src/kip.h"

Test(g_map, map_memleak)
{
    kip_map_init(0);

    kip_map_free();
}

Test(malloc, simple_malloc_leak)
{
    kip_map_init(0);

    int *x = kip_malloc(sizeof(int));

    cr_assert(x);

    kip_map_free();
}

Test(malloc, multiple_malloc_leak)
{
    kip_map_init(0);

    int *x0 = kip_malloc(sizeof(int));
    int *x1 = kip_malloc(sizeof(int));
    int *x2 = kip_malloc(sizeof(int));
    int *x3 = kip_malloc(sizeof(int));
    int *x4 = kip_malloc(sizeof(int));

    cr_assert(x0);
    cr_assert(x1);
    cr_assert(x2);
    cr_assert(x3);
    cr_assert(x4);

    kip_map_free();
}

Test(calloc, simple_calloc_leak)
{
    kip_map_init(0);

    int *x = kip_calloc(1, sizeof(int));

    cr_assert(x);

    kip_map_free();
}

Test(calloc, multiple_calloc_leak)
{
    kip_map_init(0);

    int *x0 = kip_calloc(1, sizeof(int));
    int *x1 = kip_calloc(1, sizeof(int));
    int *x2 = kip_calloc(1, sizeof(int));
    int *x3 = kip_calloc(1, sizeof(int));
    int *x4 = kip_calloc(1, sizeof(int));

    cr_assert(x0);
    cr_assert(x1);
    cr_assert(x2);
    cr_assert(x3);
    cr_assert(x4);

    kip_map_free();
}

Test(realloc, realloc_hard)
{
    kip_map_init(0);

    int *x = kip_malloc(sizeof(int) * 2);
    x = kip_realloc(x, sizeof(int) * 128);

    x[127] = 123;

    cr_assert_eq(123, x[127]);

    kip_map_free();
}

#ifdef _DBG

int main(void)
{
    kip_map_init(0);

    int *x = kip_malloc(sizeof(int));

    cr_assert(x);

    kip_map_free();
}

#endif /* ! _DBG */
