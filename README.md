# kip

KIP is a new way to handle pointers in C. No more memory leaks. No more malloc() failures.

# How to use

Initialize a kip_map with `kip_map_init()`. Flags are either 0 or LIMITED_MEMORY, if
your system is short on memory but not on CPU speed.

For each pointer, be it a calloc() or a malloc(), use `kip_malloc()` and `kip_calloc()`.

This checks for a malloc failure, and adds it to the previously created map. 
These functions return the allocated pointer so you can use them as you would normally.

When all your operations are done, call, `kip_map_free()` on the map.
